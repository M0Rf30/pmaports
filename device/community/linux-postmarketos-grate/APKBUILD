# Maintainer: Svyatoslav Ryhel <clamor95@gmail.com>

pkgname=linux-postmarketos-grate
pkgver=6.6.14
pkgrel=0
pkgdesc="Linux kernel with experimental patches for Tegra"
arch="armv7"
url="https://gitlab.com/grate-driver/linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native pmb:kconfigcheck-community"
makedepends="bash bison findutils flex postmarketos-installkernel openssl-dev
	     perl gmp-dev mpc1-dev mpfr-dev xz"

# Source
_flavor="${pkgname#linux-}"
_tag="v${pkgver//_/-}-lts"
_carch="arm"
_config="config-$_flavor.$arch"
source="$pkgname-$_tag.tar.bz2::$url/-/archive/$_tag/linux-$_tag.tar.bz2
	$_config"
builddir="$srcdir/linux-$_tag"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$CARCH" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-postmarketOS"
}

package() {
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir"/boot/dtbs
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release
}

sha512sums="
d1dd9f42cfaf64815059295b81832d250b7958fb51508707f0e292934566d936d9ae7a0ad58be1dcb8fefe9b2571b2a8ea53151715f22d11a85973e94ba7c452  linux-postmarketos-grate-v6.6.14-lts.tar.bz2
b779932789ec9b0d3741f2af264b0fb63ea9f88853de9434ccc047272b16e10a910847e02f028d7fa94e5ea684c0221699f15a589e0ba079cbfdceafddebaf1c  config-postmarketos-grate.armv7
"
