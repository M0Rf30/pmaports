# Maintainer: Oliver Smith <ollieparanoid@postmarketos.org>
# Co-Maintainer: Clayton Craft <clayton@craftyguy.net>
pkgname=postmarketos-mkinitfs
pkgver=2.3.1
pkgrel=0
pkgdesc="Tool to generate initramfs images for postmarketOS"
url="https://postmarketos.org"
depends="
	boot-deploy>=0.9
"
makedepends="go scdoc"
replaces="mkinitfs"

triggers="$pkgname.trigger=\
/lib/firmware/*:\
/usr/lib/systemd/boot:\
/usr/libexec/pmos-tests-initramfs:\
/usr/share/deviceinfo:\
/usr/share/kernel/*:\
/usr/share/mkinitfs-triggers:\
/usr/share/mkinitfs/*\
"

# mkinitfs-vendor-$pkgver.tar.gz: vendored Go deps, is part of the release:
# https://gitlab.com/postmarketOS/postmarketos-mkinitfs/-/releases
source="
	https://gitlab.com/postmarketOS/postmarketos-mkinitfs/-/archive/$pkgver/postmarketos-mkinitfs-$pkgver.tar.gz
	https://gitlab.com/api/v4/projects/postmarketOS%2Fpostmarketos-mkinitfs/packages/generic/mkinitfs-vendor-$pkgver/$pkgver/mkinitfs-vendor-$pkgver.tar.gz
	"

install="$pkgname.post-upgrade"
arch="all"
license="GPL-2.0-or-later"
provider_priority=999  # higher priority than Alpine's mkinitfs
provides="initramfs-generator"
subpackages="$pkgname-doc"

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"
export GOPATH="$srcdir"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"

prepare() {
	default_prepare
	ln -s "$srcdir"/vendor "$builddir"/vendor
}

build() {
	unset LDFLAGS  # passed to Go as linker flags, which are invalid
	make VERSION="$pkgver"
}

package() {
	make PREFIX=/usr DESTDIR="$pkgdir" install
}

check() {
	go test ./...
}

sha512sums="
76b447eb05bfed465f9c0f42d851a55249a5854c4b34c520c4bedef2d2916d1ffcac0b3273b916edacfa6397320ea1f52f4dab47a09ab385e6702c062985f8b1  postmarketos-mkinitfs-2.3.1.tar.gz
61572a14ba1c06cdff6ae80b8e4a70f332d016a73b84afaca085dcc5a5c0fe19cf70d42a6595f6d11d8928b41eb680e14e579c20e3839dd47766105f46a90e7d  mkinitfs-vendor-2.3.1.tar.gz
"
